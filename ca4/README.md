# Class Assignment 4

## Containers with Docker

This is the README for the fourth class assignment.

This class assignment subject is **Containers with Docker**.


## 1. Analysis, Design and Implementation

### Containerization

Containerization is a system of intermodal freight transport using intermodal containers, also called shipping containers and 
ISO containers.

Containerization allows developers to create and deploy applications faster and more securely. 

With traditional methods, code is developed in a specific computing environment which, when transferred to a new location, 
often results in bugs and errors.

Containerization eliminates this problem by bundling the application code together with the related configuration files, 
libraries, and dependencies required for it to run.

This single package of software or “container” is abstracted away from the host operating system, and hence, 
it stands alone and becomes portable—able to run across any platform or cloud, free of issues.

Below you can see a brief comparison between Virtual Machines and Containers.

![image](./prtsc/vm_vs_container.png)

The image bellow shows you the workflow of the process behind the container creation:

![image](./prtsc/theory_what_is_happening.png)


### Ubuntu

You will see that in the Dockerfiles below, we set our WORKDIR's to **/usr** inside our Ubuntu machines.

That's because **/usr** is a place for system-wide, read-only files, so all your installed software goes there. 

It does not duplicate any names of **/** except **/bin** and **/lib**, but originally with a different purpose: 
**/bin** and **/lib** are only for binaries and libraries required for booting, 
while **/usr/bin** and **/usr/lib** are for all the other executables and libraries. 

### Docker Desktop

You can download the latest **Docker Desktop** version here:

https://www.docker.com/products/docker-desktop

Before installing **Docker Desktop** you need to get your Ubuntu to use wsl version 2.

To do that you need to have virtualization allowed in your operative system and in your BIOS.

For Windows 10, we need to do the following in order to allow our Ubuntu system to use wsl version 2.

In my case I already add Ubuntu installed and it was running with wsl version 1:

![image](./prtsc/ubuntu_wsl_1.png)

1) Open PowerShell as admin and enter the following commands:

1.a) In order to activate virtualization in your windows 10 system:

```shell
dism.exe /online /enable-feature /featurename:VirtualMachinePlatform /all /norestart
```

1.b) In order to define Ubuntu as your default wsl distro:
```shell
wsl --set-default ubuntu
```

1.c) To set your Ubuntu wsl version as 2:

```shell
wsl.exe --set-version Ubuntu 2
```

1.d) To set your wsl default version as 2:

```shell
wsl.exe --set-version Ubuntu 2
```

1.e) To check if everything went as expected:

```shell
wsl --list --verbose
```

![image](./prtsc/final_wsl_2.png)

Then you can install **Docker Desktop** and select ubuntu as its wsl distro:

![image](./prtsc/docker_wsl2_integration.png)


### Issues

Optionally, you can create issues on Bitbucket to track the development of this class assignment. They could represent software features, story leads, user stories, etc.

For the development of this assigment, 5 issues can be created:

1. Use docker-composer to create db and web containers;
2. Publish db and web images to Docker Hub;
3. Use a volume with the db container to get a copy of the database file;
4. Describe the process in the readme file for this assignment;
5. At the end of this assignment mark your repository with the tag ca4.

### Use docker-composer to create db and web containers (Issue 1)

To use the **docker-composer** command to create our db and web containers, we need to have a **docker-compose.yml** file in our app
folder.

Create one with the following content:

```yml
version: '3'
services:
  web:
    build: web
    ports:
      - "8080:8080"
    networks:
      default:
        ipv4_address: 192.168.33.10
    depends_on:
      - "db"
  db:
    build: db
    ports:
      - "8082:8082"
      - "9092:9092"
    volumes:
      - ./data:/usr/src/data
    networks:
      default:
        ipv4_address: 192.168.33.11
networks:
  default:
    ipam:
      driver: default
      config:
        - subnet: 192.168.33.0/24

```

In this file, we are declaring that we will create two containers: **web** and **db**.

In the **web** container:

- We are portforwarding port 8080 from host to guest;
- We are fixing its ipv4 address as: 192.168.33.10;
- And we are saying that this containers depends on the **db** container.

In the **db** container:

- We are portforwarding port 8082 and 9092 from host to guest;
- We are creating a shared folder between our **data** folder in the same path as the **docker-composer.yml** file and the
**data** folder in **/usr/src/** in our container;
- We are fixing its ipv4 address as: 192.168.33.10;
- And we are saying that this containers depends on the **db** container.

Finally we are creating a network infrastructure with the subnet 192.168.33.0/24.

After this, we need to create a **Dockerfile** for each of our container, inside the respective folder.

The **web** container **Dockerfile** has the following content:

```shell
FROM tomcat

RUN apt-get update -y

RUN apt-get install -f

RUN apt-get install git -y

RUN apt-get install nodejs -y

RUN apt-get install npm -y

RUN mkdir -p /tmp/build

WORKDIR /tmp/build/

RUN git clone https://RicardoNunes230@bitbucket.org/RicardoNunes230/devops-20-21-1201781.git

WORKDIR /tmp/build/devops-20-21-1201781/ca3/part2/gradle-tut-basic/

RUN chmod +x gradlew

RUN ./gradlew clean build

RUN cp build/libs/gradle-tut-basic-0.0.1-SNAPSHOT.war /usr/local/tomcat/webapps/

EXPOSE 8080
```

This will install the needed applications for your container to be run, then create the **/tmp/build** folder and change
the workdir to it.

Then it will clone our repository to the **/tmp/build/devops-20-21-1201781/ca3/part2/gradle-tut-basic/** directory and 
run the command **"chmod +x gradlew"** to avoid the following error:

![image](./prtsc/first_error.png)

After that it will run the **"./gradlew clean build"** and the **"cp build/libs/gradle-tut-basic-0.0.1-SNAPSHOT.war /usr/local/tomcat/webapps/"** commands.

Finally it will expose the 8080 port.

The **db** container **Dockerfile** has the following content:

```shell
FROM ubuntu

RUN apt-get update && \
  apt-get install -y openjdk-8-jdk-headless && \
  apt-get install unzip -y && \
  apt-get install wget -y

RUN mkdir -p /usr/src/app

WORKDIR /usr/src/app/

RUN wget https://repo1.maven.org/maven2/com/h2database/h2/1.4.200/h2-1.4.200.jar

EXPOSE 8082
EXPOSE 9092

CMD java -cp ./h2-1.4.200.jar org.h2.tools.Server -web -webAllowOthers -tcp -tcpAllowOthers -ifNotExists

```

This will install the needed applications for your container to be run, then create the **/usr/src/app** folder and change
the workdir to it.

Then it will run the command **"wget https://repo1.maven.org/maven2/com/h2database/h2/1.4.200/h2-1.4.200.jar"**.

After that it will expose the 8082 and 9092 ports.

Finally, when the container is ready, it will run the **"java -cp ./h2-1.4.200.jar org.h2.tools.Server -web -webAllowOthers -tcp -tcpAllowOthers -ifNotExists"** command.

In order to create the containers, just enter the following command in the folder where **docker-compose.yml** is:

```shell
docker-compose up
```

You can check your results in:

**localhost:8080/gradle-tut-basic-0.0.1-SNAPSHOT**

Like this:

![image](./prtsc/docker_website.png)


### Publish db and web images to Docker Hub (Issue 2)

First you need to create an account in **Docker Hub**:

https://hub.docker.com/

After that you need to run the following command:

```shell
docker login -u <your_username> -p <your_password> docker.io
```

Like this:

![image](./prtsc/docker_login.png)

Then, you can simply **tag** and **push** your containers.

To **tag**:

```shell
docker tag <container>:<tag> <username>/<container>:<tag>
```

To **push**:

```shell
docker push <username>/<container>:<tag>
```

For the **web** container it should look like this:

![image](./prtsc/docker_tag_and_push_web.png)

For the **db** container it should look like this:

![image](./prtsc/docker_tag_and_push_db.png)

Then it will appear in your **Docker Hub**:

![image](./prtsc/docker_hub.png)

### Use a volume with the db container to get a copy of the database file (Issue 3)

You can do this by using the **exec** to run a shell in the container and copying the database file to the volume.

Since we already have a volume created by our **docker-compose**, we only need to copy the database file inside our container
to our shared folder.

Currently our folder has no database file:

![image](./prtsc/before_copy.png)

After running the shown command:

```shell
docker exec docker_db_1 cp jpa.mv.db ../data
```

We will execute the command **"cp jpa.mv.db ../data"** inside the container called **docker_db_1**.

This will copy the **jpa.mv.db** file do the **data** folder in the folder that precedes the path where the WORKDIR currently is.

![image](./prtsc/after_copy.png)

You can see that after copying the database file, it is shown in our host due to the volume that permits this file to be shared between
the host and the container.


### Mark your repository with the tag ca4 (Issue 5)

After finishing the assignment and completing the README file, the repository should be market with a new tag "ca4".

We can do this using the following **Git** commands:

```shell
git tag -a ca4 -m "End of ca4 assignment"

git push origin ca4
```

## 2. Analysis of an Alternative

x

## 3. Implementation of the Alternative

Keep in mind that **Heroku** is not an alternative to **Docker** but a way to deploy your containers into the cloud.

Most unfortunately I was not able to deploy the containerized application to **Heroku**, due to an "Application Crashed" error when I tried to do so:

![image](./prtsc/heroku_application_error.png)

And the logs are:

![image](./prtsc/heroku_application_logs.png)

I've tried following many of the online guides and figuring out the error by reading the documentations and just generally searching online for similar
problems, but, alas, it was to no avail.

Therefore, just in order to deploy the application to the cloud, I tried to deploy the **.war** file that results on the application build. And it worked.

### Download and install Heroku

First, we must create an account on **Heroku**, through this link:

https://www.heroku.com

Then you need to download **Heroku CLI** here:

https://devcenter.heroku.com/articles/heroku-cli

Lastly, just follow the instructions to  install it and it should be ready to go.


### Procfile

For **Heroku** to know which file will be executed, we have to create a text file called **Procfile** (without extension)
that must contain:

```shell
web java $JAVA_OPTS -jar webapp-runner.jar ${WEBAPP_RUNNER_OPTS} --port $PORT ./build/libs/tut-basic-gradle-0.0.1-SNAPSHOT.war
```

### Update app.js

We must make changes so that http requests are correctly made:

```shell
	componentDidMount() { // <2>
		client({method: 'GET', path: '/api/employees'}).done(response => {
			this.setState({employees: response.entity._embedded.employees});
		});
	}
```

### Update application.properties

In the application.properties we have to enable the database in memory:

```shell
...
spring.datasource.url=jdbc:h2:mem:jpadb
#spring.datasource.url=jdbc:h2:tcp://192.168.33.11:9092/./jpadb;DB_CLOSE_DELAY=-1;DB_CLOSE_ON_EXIT=FALSE
...
```

### Create an App at Heroku

To create a new application at **Heroku**, just select the **"Create new app"** option and enter the app name and choose a region.

It should look like this:

![image](./prtsc/heroku_create_app.png)

### Deploy the application

In the terminal, at the base of the project we will log in to Heroku:

```shell
heroku login
```

It should look like this:

![image](./prtsc/heroku_login.png)

Then you need to click on **"Log in to the Heroku CLI"** and it should log you in.

It should look like this:

![image](./prtsc/heroku_logged_in.png)

Next, let's build the project:

```shell
./gradlew build
```

To deploy the war file created in the build we have to install the Heroku Java CLI plugin:

```shell
heroku plugins:install java
```

Now we just need to deploy, using the following command:

```shell
heroku war:deploy <path_to_war_file> --app <app_name>
```

It should look like this:

![image](./prtsc/heroku_war_deploy.png)

### Check the results

The site is live here:

https://devops-ca4-tut-basic.herokuapp.com/

It should look like this:

![image](./prtsc/heroku_website.png)