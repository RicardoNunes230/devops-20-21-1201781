# Class Assignment 3 - Part 2

## Virtualization with Vagrant

This is the README for the third class assignment.

This class assignment subject is **Virtualization with Vagrant**.

On this second part of the third assignment, the goal is to setup a virtual environment to execute the tutorial spring boot 
application, gradle "basic" version (developed in CA2, Part2).

The vagrantfiles are located inside "./vagrant". There you can find both VirtualBox and Hyper-V vagrantfiles.

## 1. Analysis, Design and Implementation

## Issues

Optionally, you can create issues on Bitbucket to track the development of this class assignment. They could represent software features, story leads, user stories, etc.

For the development of this assigment, 8 issues can be created:

1. Get Vagrantfile from https://bitbucket.org/atb/vagrant-multi-spring-tut-demo/;
2. Study the Vagrantfile and see how it is used to create and provision 2 VMs (web and db);
3. Copy this Vagrantfile to your repository;
4. Update the Vagrantfile configuration so that it uses your own gradle version of the spring application;
5. Check https://bitbucket.org/atb/tut-basic-gradle to see the changes necessary so that the spring application uses the H2 server in the db VM;
6. Replicate the checked changes;
7. Describe the process in the readme file for this assignment;
8. At the end of the part 2 of this assignment mark your repository with the tag ca3-part2.

## Get the Vagrantfile (Issue 1)

You can get the Vagrantfile by cloning or downloading the following repository:

https://bitbucket.org/atb/vagrant-multi-spring-tut-demo/

## Study the Vagrantfile (Issue 2)

If we run the command vagrant up, we will have two VMs (Virtual Machine):

**web**: this VM is used to run tomcat and the spring boot basic application

**db**: this VM is used to execute the H2 server database

### Common provision analysis

```Ruby
config.vm.provision "shell", inline: <<-SHELL
    sudo apt-get update -y
    sudo apt-get install iputils-ping -y
    sudo apt-get install -y avahi-daemon libnss-mdns
    sudo apt-get install -y unzip
    sudo apt-get install openjdk-8-jdk-headless -y
    # ifconfig
SHELL
```

The previous provision is common for both VMs, it will install IP utils, Avahi Daemon, Unzip and JDK.

### Database provision analysis

```Ruby
config.vm.define "db" do |db|
    db.vm.box = "envimation/ubuntu-xenial"
    db.vm.hostname = "db"
    db.vm.network "private_network", ip: "192.168.33.11"
```

This box will install Ubuntu 16.04.7 LTS (Xenial Xerus).

The hostname will be "db".

The IP address configured for this VM will be "192.168.33.11". The database port will be "9092".


```Ruby
db.vm.network "forwarded_port", guest: 8082, host: 8082
db.vm.network "forwarded_port", guest: 9092, host: 9092
```

Here, we forwarded the guest port "8082" to the host port "8082" and the guest port "9092" to the host port "9092".

We did that because we want to access H2 console from the host using port "8082" and to connet to the H2 server using port "9092"

Now we want the H2 database to be installed only in the first time the "vagrant up" command is used. We can do it with the following provision:

```Ruby
db.vm.provision "shell", inline: <<-SHELL
    wget https://repo1.maven.org/maven2/com/h2database/h2/1.4.200/h2-1.4.200.jar
SHELL
```

However, the following provision shell will always run, in order to execute the H2 server process, using java. This is achieved by applying the "always" flag.

```Ruby
db.vm.provision "shell", :run => 'always', inline: <<-SHELL
    java -cp ./h2*.jar org.h2.tools.Server -web -webAllowOthers -tcp -tcpAllowOthers -ifNotExists > ~/out.txt &
SHELL
```

### Web server provision analysis

```Ruby
config.vm.define "web" do |web|
	web.vm.box = "envimation/ubuntu-xenial"
	web.vm.hostname = "web"
	web.vm.network "private_network", ip: "192.168.33.10"
```

This box will install Ubuntu 16.04.7 LTS (Xenial Xerus).

The hostname will be "db".

The IP address configured for this VM will be "192.168.33.10". The tomcat will be accessible through "8080" port.

```Ruby
web.vm.provider "virtualbox" do |v|
	v.memory = 1024
end
```

Here we instructed the virtualization software to allocate "1024" megabytes (1GB)s of memory to this virtual machine.

```Ruby
web.vm.network "forwarded_port", guest: 8080, host: 8080
```

Here, we forwarded the guest port "8080" to the host port "8080".

We did that because we want to access tomcat from the host using port "8080".

```Ruby
web.vm.provision "shell", inline: <<-SHELL, privileged: false
	sudo apt-get install git -y
	sudo apt-get install nodejs -y
	sudo apt-get install npm -y
	sudo ln -s /usr/bin/nodejs /usr/bin/node
	sudo apt install tomcat8 -y
	sudo apt install tomcat8-admin -y
	# If you want to access Tomcat admin web page do the following:
	# Edit /etc/tomcat8/tomcat-users.xml
	# uncomment tomcat-users and add manager-gui to tomcat user

	# Change the following command to clone your own repository!
	git clone https://atb@bitbucket.org/atb/tut-basic-gradle.git
	cd tut-basic-gradle
	chmod u+x gradlew
	./gradlew clean build
	# To deploy the war file to tomcat8 do the following command:
	sudo cp ./build/libs/basic-0.0.1-SNAPSHOT.war /var/lib/tomcat8/webapps
SHELL
```

The previous provision will install git, nodejs, npm and tomcat8.

After that it will clone a git repository (in this case https://atb@bitbucket.org/atb/tut-basic-gradle.git).

Then it will change its directory to "tut-basic-gradle" and give all permisions to "gradlew" in the current user.

It will then run "./gradlew clean build" to make a clean build and copy the resulting "war" file from "./build/libs/basic-0.0.1-SNAPSHOT.war" to "/var/lib/tomcat8/webapps".


## Copy the Vagrantfile to your repository (Issue 3)

You may now copy the Vagrantfile to your repository (make sure you don't copy the .git folder).

## Update the Vagrantfile configuration (Issue 4)

You need to update the Vagrantfile configuration in order to make it compatible with your project and repository.

Simply change the lines 70, 71 and 75 of the Vagrantfile to clone your own repository, change the VM directory to the right place and match the name of the built "war" file.

It should look like the following:

![image](./prtsc/lines_70_71_75.png)

## Changed the tut-basic-gradle application (Issue 5)

### build.gradle changes

First, in the "build.gradle" file, under the "plugins", add:

```
id 'war'
```
 
After that, in the "build.gradle" file, under "dependencies", add:

```
providedRuntime 'org.springframework.boot:spring-boot-starter-tomcat'
```

Finally you have to change the java version in your "build.gradle" file to 1.8, by adding the following lines:

```
sourceCompatibility = 1.8
targetCompatibility = 1.8
```

It should look like this:

![image](./prtsc/buildgradle_file.png)

### ServletInitializer.java creation

Then, create the "ServletInitializer.java" class.

It should have the following content:

```
package com.greglturnquist.payroll;

import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;

public class ServletInitializer extends SpringBootServletInitializer {

    @Override
    protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
        return application.sources(ReactAndSpringDataRestApplication.class);
    }

}
```

### application.properties changes
  
After that, insert the following lines to "src/main/resources/application.properties" file to enable support for the H2 console:

```
server.servlet.context-path=/gradle-tut-basic-0.0.1-SNAPSHOT
spring.data.rest.base-path=/api
#spring.datasource.url=jdbc:h2:mem:jpadb
spring.datasource.url=jdbc:h2:tcp://192.168.33.11:9092/./jpadb;DB_CLOSE_DELAY=-1;DB_CLOSE_ON_EXIT=FALSE
spring.datasource.driverClassName=org.h2.Driver
spring.datasource.username=sa
spring.datasource.password=
spring.jpa.database-platform=org.hibernate.dialect.H2Dialect
spring.jpa.hibernate.ddl-auto=update
spring.h2.console.enabled=true
spring.h2.console.path=/h2-console
spring.h2.console.settings.web-allow-others=true
```

### app.js changes

Don't forget to change the application context path to match the project build. You can do it in "src/main/js/app.js", like this:

```
componentDidMount() { // <2>
	client({method: 'GET', path: '/gradle-tut-basic-0.0.1-SNAPSHOT/api/employees'}).done(response => {
		this.setState({employees: response.entity._embedded.employees});
	});
}
```

It should look like the following:

![image](./prtsc/componentDidMount.png)

### index.html changes
  
Then you need to remove the backslash on line 6 of the "main/resources/templates/index.html" file.

It should look like this:

![image](./prtsc/index_html.png)

### Build and commit

You can now build your project and commit the changes to your repository.

## Replicate the checked changes (Issue 6) and VM implementation process (Issue 7)

### Repository privacy

If your remote repository is private, the following error will occur if you try to create and configure your guest machines according to your Vagrantfile:

![image](./prtsc/password_error.png)

Therefore, you need to make your remote repository public, like this:

![image](./prtsc/public_repository.png)

As you can see, the "This is a private repository" box is unchecked.

### gradle-wrapper.jar forced commit

Since your remote repository probably is ignoring any ".jar" files, your "gradle-wrapper.jar" is not getting up to your repository.

That may cause the following error in your VM when you try to create and configure your guest machines according to your Vagrantfile:

![image](./prtsc/gradle_wrapper_error.png)

In order to force your "gradle-wrapper.jar" into your repository and commit it, run the following commands:

```bash
git add -f gradle-wrapper.jar

git commit -m "Force add on gradle-wrapper.jar"

git push origin master
```

Note: "git add -f **gradle-wrapper.jar**" should have your "gradle-wrapper.jar" path (in the bold part) if you are not inside its directory (in my case, "devops-20-21-1201781/ca3/part2/gradle-tut-basic/gradle/wrapper").

### Create and configure the guest machines according to the Vagrantfile

Finally, in order to replicate the checked changes in Issue 5, just run the following command in the directory where you have your Vagrantfile:

```bash
vagrant up
```

### Expected result

If all goes well, you can go to the following link in order to see your website up and running:

http://192.168.33.10:8080/gradle-tut-basic-0.0.1-SNAPSHOT/

The result should be the following:

![image](./prtsc/frontend_working.png)

## Mark your repository with the tag ca3-part2 (Issue 8)

After finalizing the assignment and completing the README file, the repository should be market with a new tag "ca3-part2".

We can do this using the following **Git** commands:

```
git tag -a ca3-part2 -m "End of ca3-part2 assignment"

git push origin ca3-part2
```

## 2. Analysis of an Alternative

The chosen alternative was **Hyper-V**.

A little more about **Hyper-V**:

- Is a type 1 hypervisor, meaning virtual machines are always running as long as the hardware is;
- It integrates well with Windows infrastructures, and is simple to use once it has been implemented.

Its disadvantages when compared to **VirtualBox**:
- Isn’t as easy to set up as Oracle VM VirtualBox;
- Can only be used on Windows Pro machines;
- Can't configure static IP's for it's Virtual Machines, whereas VirtualBox can.

## 3. Implementation of the Alternative

### Activate Hyper-V

The first thing you need to do is activate **Hyper-V**.

You can do it by going to the **PowerShell** and running the following command:

```shell
Enable-WindowsOptionalFeature -Online -FeatureName Microsoft-Hyper-V -All
```

It should look like this:

![image](./prtsc/hyperv_powershell_enable_hyperv.png)

You can then access it through the start menu:

![image](./prtsc/hyperv_open.png)

### Vagrantfile for Hyper-V

This is your Vagrantfile for Hyper-V:

```Ruby
Vagrant.configure("2") do |config|
  config.vm.box = "bento/ubuntu-16.04"
  config.vm.provider "hyperv"
  config.vm.synced_folder '.', '/vagrant', disabled: true

  # This provision is common for both VMs
  config.vm.provision "shell", inline: <<-SHELL
    sudo apt-get update -y
    sudo apt-get install iputils-ping -y
    sudo apt-get install -y avahi-daemon libnss-mdns
    sudo apt-get install -y unzip
    sudo apt-get install openjdk-8-jdk-headless -y
    # ifconfig
  SHELL

  #============
  # Configurations specific to the database VM
  config.vm.define "db" do |db|
    db.vm.box = "bento/ubuntu-16.04"
    db.vm.hostname = "db"
    db.vm.network "private_network", ip: "192.168.33.11"

    # We want to access H2 console from the host using port 8082
    # We want to connet to the H2 server using port 9092
    db.vm.network "forwarded_port", guest: 8082, host: 8082
    db.vm.network "forwarded_port", guest: 9092, host: 9092

    # We need to download H2
    db.vm.provision "shell", inline: <<-SHELL
      wget https://repo1.maven.org/maven2/com/h2database/h2/1.4.200/h2-1.4.200.jar
    SHELL

    # The following provision shell will run ALWAYS so that we can execute the H2 server process
    # This could be done in a different way, for instance, setiing H2 as as service, like in the following link:
    # How to setup java as a service in ubuntu: http://www.jcgonzalez.com/ubuntu-16-java-service-wrapper-example
    #
    # To connect to H2 use: jdbc:h2:tcp://192.168.33.11:9092/./jpadb
    db.vm.provision "shell", :run => 'always', inline: <<-SHELL
      java -cp ./h2*.jar org.h2.tools.Server -web -webAllowOthers -tcp -tcpAllowOthers -ifNotExists > ~/out.txt &
    SHELL
  end

  #============
  # Configurations specific to the webserver VM
  config.vm.define "web" do |web|
    web.vm.box = "bento/ubuntu-16.04"
    web.vm.hostname = "web"
    web.vm.network "private_network", ip: "192.168.33.10"

    # We set more ram memmory for this VM
    web.vm.provider "hyperv" do |h|
      h.memory = 1024
    end

    # We want to access tomcat from the host using port 8080
    web.vm.network "forwarded_port", guest: 8080, host: 8080

    web.vm.provision "shell", inline: <<-SHELL, privileged: false
      sudo apt-get install git -y
      sudo apt-get install nodejs -y
      sudo apt-get install npm -y
      sudo ln -s /usr/bin/nodejs /usr/bin/node
      sudo apt install tomcat8 -y
      sudo apt install tomcat8-admin -y
      # If you want to access Tomcat admin web page do the following:
      # Edit /etc/tomcat8/tomcat-users.xml
      # uncomment tomcat-users and add manager-gui to tomcat user

      # Change the following command to clone your own repository!
      git clone https://RicardoNunes230@bitbucket.org/RicardoNunes230/devops-20-21-1201781.git
      cd devops-20-21-1201781/ca3/part2/gradle-tut-basic-hyperv
      chmod u+x gradlew
      ./gradlew clean build
      # To deploy the war file to tomcat8 do the following command:
      sudo cp ./build/libs/gradle-tut-basic-0.0.1-SNAPSHOT.war /var/lib/tomcat8/webapps
    SHELL

  end

end
```

As you can see:

```Ruby
config.vm.provider "hyperv"
```

The default virtualization software is now Hyper-V.

```Ruby
config.vm.box = "bento/ubuntu-16.04"
```

Both VM's are now instaling bento/ubuntu-16.04 box.

```
cd devops-20-21-1201781/ca3/part2/gradle-tut-basic-hyperv
```

The VM now changes to a specific "gradle-tut-basic" project made just for hyper-v (more on that later).

The project can be cloned/downloaded in my public repository.

### Setup VM

You can create your virtual machines by running inside your vagrantfile for your **Hyper-V** folder:

```
vagrant up
```

You should see this:

![image](./prtsc/hyperv_vagrant_up.png)

And the following result can be seen in the application:

![image](./prtsc/hyperv_vagrant_up_result.png)

Here you can see the instalation process:

![image](./prtsc/hyperv_vagrantup.png)

### Gradle project for hyper-v and other differences

The difference between this project and the regular gradle-tut-basic project is the IP of "spring.datasource.url" in "application.properties".

Instead, you could manually change your IP inside the virtual machine local repository and then make a clean build and copying the resulting ".war" file to
"/var/lib/tomcat8/webapps".

I prefer this to change manually the IP inside the repository because this way, if your application needs to be rebuilt, il will mantain its IP (as long as the machine
IP does not change).

This needed to be changed because we can't define the IP of the "db" virtual machine while using **Hyper-V**.

In my case, it was dynamically attributed "172.25.109.209:9092".

Therefore we need to update our "spring.datasource.url" to use the correct IP.

You can see the changes here:

![image](./prtsc/hyperv_ip_before.png)

_Before_

![image](./prtsc/hyperv_ip_after.png)

_After_

You should change this IP to whatever IP is attributed to your "db" virtual machine.

Then you must add, commit and push your project to the remote repository.

After that you must delete your local repository inside your VM, like this:

![image](./prtsc/hyperv_vagrant_ssh_web_delete_repos.png)

In the above printscreen you can see the commands to log in the virtual machine through ssh and how to delete the local repository.

Finally you need to reload vagrant with the following command:

```
	vagrant reload --provision
```

You should see the following:

![image](./prtsc/hyperv_vagrant_reload_provision.png)

And the following result:

![image](./prtsc/hyperv_vagrant_reload_provision_result.png)

### Final result

You can now see your website up and running in your dynamically generated IP's by **Hyper-V**. In my case it was "172.25.97.170:8080".

It should look like this:

![image](./prtsc/hyperv_frontend_working.png)