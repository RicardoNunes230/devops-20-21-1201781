# README #

Repository created by Ricardo Nunes (1201781) for DevOps course unit.

### Project paths ###

* [Class Assignment 1](https://bitbucket.org/RicardoNunes230/devops-20-21-1201781/src/master/ca1/)

Project folder that contains the "tut-basic" project given during the first lesson in order to create and use the git repository, in order to evaluate my Version Control knowledge.

* [Class Assignment 2 - Part 1](https://bitbucket.org/RicardoNunes230/devops-20-21-1201781/src/master/ca2/part1/)

Project folder that contains the "gradle_basic_demo" project given during the fifth lesson in order to evaluate my Build Tools knowledge.

* [Class Assignment 2 - Part 2](https://bitbucket.org/RicardoNunes230/devops-20-21-1201781/src/master/ca2/part2/)

Project folder that contains the "tut-basic" project given during the first lesson, now built using **Gradle**, in order to evaluate my Build Tools knowledge.

* [Class Assignment 3 - Part 1](https://bitbucket.org/RicardoNunes230/devops-20-21-1201781/src/master/ca3/part1)

Project folder that contains the README.md file written in order to evaluate my Virtualization knowledge.

* [Class Assignment 3 - Part 2](https://bitbucket.org/RicardoNunes230/devops-20-21-1201781/src/master/ca3/part2)

Project folder that contains the Vagrant setup to a virtual environment to execute the "tutorial spring boot application" and "gradle basic" projects 
in order to evaluate my Virtualization knowledge. Both projects have their own VM-ready repositories in this folder.

* [Class Assignment 4](https://bitbucket.org/RicardoNunes230/devops-20-21-1201781/src/master/ca4)

Project folder that contains all the docker files and the README.md file written in order to evaluate my Containerization knowledge.

* [Class Assignment 5 - Part 1](https://bitbucket.org/RicardoNunes230/devops-20-21-1201781/src/master/ca5/part1)

Project folder that contains the README.md file written in order to evaluate my CI/CD pipelines with Jenkins knowledge.

* [Class Assignment 5 - Part 2](https://bitbucket.org/RicardoNunes230/devops-20-21-1201781/src/master/ca5/part2)

Project folder that contains all the project files and the README.md file written in order to evaluate my CI/CD pipelines with Jenkins, using also
Docker images, knowledge.

### Contacts ###

* Former student email: 1201781@isep.ipp.pt
* Personal email: ricardoffnunes@gmail.com