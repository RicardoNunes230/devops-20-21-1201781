package com.greglturnquist.payroll;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class EmployeeTest {

    @Test
    void createEmployee_Successfully(){
        Employee employee = new Employee("first name", "last name", "description", "job title", "email@email.com");
        assertNotNull(employee);
        assertEquals("first name", employee.getFirstName());
        assertEquals("last name", employee.getLastName());
        assertEquals("description", employee.getDescription());
        assertEquals("job title", employee.getJobTitle());
        assertEquals("email@email.com", employee.getEmail());
    }

    @Test
    void createEmployee_Unsuccessfully_Null_FirstName(){
        assertThrows(IllegalArgumentException.class, () ->
            new Employee(null, "last name", "description", "job title", "email@email.com"));
    }

    @Test
    void createEmployee_Unsuccessfully_Null_LastName(){
        assertThrows(IllegalArgumentException.class, () ->
                new Employee("first name", null, "description", "job title", "email@email.com"));
    }

    @Test
    void createEmployee_Unsuccessfully_Null_Description(){
        assertThrows(IllegalArgumentException.class, () ->
                new Employee("first name", "last name", null, "job title", "email@email.com"));
    }

    @Test
    void createEmployee_Unsuccessfully_Null_JobTitle(){
        assertThrows(IllegalArgumentException.class, () ->
                new Employee("first name", "last name", "description", null, "email@email.com"));
    }

    @Test
    void createEmployee_Unsuccessfully_Null_Email(){
        assertThrows(IllegalArgumentException.class, () ->
                new Employee("first name", "last name", "description", "job title", null));
    }

    @Test
    void createEmployee_Unsuccessfully_InvalidFormat_Email(){
        assertThrows(IllegalArgumentException.class, () ->
                new Employee("first name", "last name", "description", "job title", "hello"));
    }

}