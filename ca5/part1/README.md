# Class Assignment 5

## CI/CD Pipelines with Jenkins

This is the README for the part one of the fifth class assignment.

This class assignment subject is **CI/CD Pipelines with Jenkins**.

## 1. Analysis, Design and Implementation

### Continuous Integration/Continuous Deployment

### Jenkins

Jenkins is an open-source Continuous Integration server written in Java for orchestrating a chain of actions to achieve the 
Continuous Integration process in an automated fashion. 
Jenkins supports the complete development life cycle of software from building, testing, documenting the software, 
deploying, and other stages of the software development life cycle.

### Issues

Optionally, you can create issues on Bitbucket to track the development of this class assignment. They could represent software features, story leads, user stories, etc.

For the development of this assigment, 5 issues can be created:

1. Create a new pipeline in jenkins;
2. Configure the jenkinsfile script;
3. Run the pipeline;
4. Describe the process in the readme file for this assignment;
5. At the end of this assignment mark your repository with the tag ca5-part1.

### Create a pipeline (Issue 1)

For this step we'll be using the example that was provided by the teacher in the first Jenkins lecture.

First, we need to install **Jenkins** in our computer and run it. 

You can use the official page to guide you: https://www.jenkins.io/doc/book/installing/

In my case I installed through docker:

![image](./prtsc/jenkins_install_command.png)

Then you need to create an admin...

![image](./prtsc/jenkins_install_create_first_admin_user.png)

...and a password.

![image](./prtsc/jenkins_install_generated_password.png)

Jenkins will then install:

![image](./prtsc/jenkins_install_getting_started.png)

And give you an url and a confirmation that it is ready:

![image](./prtsc/jenkins_install_instance_configuration.png)

![image](./prtsc/jenkins_install_ready.png)

Now you need to configure your pipeline, like the following:

![image](./prtsc/jenkins_pipeline_configuration.png)

You should create your credentials to access the targeted repository.

You should also use the "Script Path" to target your **Jenkinsfile** inside your project. 
The chosen **Jenkinsfile** will be used to run your pipeline.

### Edit the jenkinsfile (Issue 2)

Create a jenkinsfile like the following:

```shell

pipeline {
    agent any

    stages {
        stage('Checkout') {
            steps {
                echo 'Checking out...'
                git credentialsId: 'devops_1201781_credentials', url: 'https://RicardoNunes230@bitbucket.org/RicardoNunes230/devops-20-21-1201781.git'
            }
        }
        
        stage('Assemble') {
            steps {
				dir('ca2/part1/gradle_basic_demo/') {
					echo 'Assembling...'
					script{
						if (isUnix()){
							sh 'chmod +x ./gradlew'
							sh './gradlew clean assemble'
						}
						else{
							bat './gradlew clean assemble'
						}
					}
                }
            }      
        }
		
		stage('Archiving') {
            steps {
				dir('ca2/part1/gradle_basic_demo/') {
					echo 'Archiving...'
                    archiveArtifacts 'build/libs/*'
				}
            }     
        }
        
        stage('Test') {
            steps {
				dir('ca2/part1/gradle_basic_demo/') {
                    echo 'Testing...'
					script{
						if (isUnix()){
							sh './gradlew clean test'
							junit '**/TEST-basic_demo.AppTest.xml'
						}
						else{
							bat './gradlew clean test'
							junit '**/TEST-basic_demo.AppTest.xml'
						}
					}
                }
            }     
        }
        

    }
}

```

This jenkinsfile should be placed where you declared your pipeline jenkinsfile path.

This will create 4 stages:
1. Checkout - Will checkout to your git repository using the given credentials;
2. Assemble - Will assemble a clean build of the project;
3. Archiving - Will archive the generated artifact;
4. Test - Will execute clean tests and save its report.

### Run the pipeline (Issue 3)

Now you can run your pipeline by entering it and clicking "Build now" in the sidebar:

![image](./prtsc/jenkins_build_now.png)

The results will be shown to you like in the printscreen above.


### Mark your repository with the tag ca5-part1 (Issue 5)

After finishing the assignment and completing the README file, the repository should be market with a new tag "ca5-part1".

We can do this using the following **Git** commands:

```shell
git tag -a ca5-part1 -m "End of ca5-part1 assignment"

git push origin ca5-part1
```